################################################################################
#                                                                              #
# Purpose:       Pankreas - Resektion                                          #
#                                                                              #
# Author:        Christian Staf                                                #
# Contact:       Email: Christian.staf@rccvast.se                              #
# Client:        Christian Staf                                                #
#                                                                              #
# Code created:  2017-03-07                                                    #
# Last updated:  2017-03-07                                                    #
# Source:        /Users/christianstaf/Documents/Bitbucket/Repositories/varden  #
#                -i-siffror/Mallar/Pankreas                                    #
#                                                                              #
# Comment:                                                                     #
#                                                                              #
################################################################################

########################## Hjälpfunktion för position ##########################
is.inca <- function() {
  env <-
    if (Sys.info()["nodename"] == "EXT-R27-PROD")
      "PROD"
  else if (Sys.info()["nodename"] == "EXT-R37-TEST")
    "TEST"
  else
    "LOCAL"
  return(env)
}

########## Lokala förberedelser samt inladdning av funktioner på INCA ########## 
if (is.inca() == "LOCAL") {
  setwd("~/Documents/Bitbucket/Repositories/varden-i-siffror/Mallar/Pankreas")
  if (!file.exists("df.rda")) {
    df <- read.csv2("df.txt")
    save(df, file = "df.rda")
  } else {
    load("df.rda")
  }
  param  <- list(start = c("2008-01-01", "2009-01-01", "2010-01-01", "2011-01-01", "2012-01-01","2013-01-01","2014-01-01", "2015-01-01"), 
                 slut = c("2008-12-31", "2009-12-31", "2010-12-31", "2011-12-31", "2012-12-31","2013-12-31","2014-12-31", "2015-12-31") ,
                 KON = 0)
}

# Namn p? register: Nationellt kvalitetsregistrer för bukspottkörtelcancer (pankreas)
# Namn p? indikator: Resektion
# Namn p? vy indikator skall basras p?: EjID_AllaForm_Nationell

################################ Ladda in paket ################################
library(dplyr)
library(incavis)

################################################################################
#                                                                              #
#                   Beräkning av urvalsramen för indikatorn                    #
#                                                                              #
################################################################################
df<-df[!duplicated(df),]
df<-df[!is.na(df$C10_opdat),]
df <- subset(df, C20_resekt_Beskrivning %in% c("Ja", "Nej"))

################################################################################
#                                                                              #
#                        Skapa variabler med mätvärden                         #
#                                                                              #
################################################################################
df$varinterest <- ifelse(df$C20_resekt_Beskrivning == "Ja", 1, 0)
df$indikator_datum <- df$C10_opdat
df$lkf <- df$A00_lkfdia

########### Skapa en ny dataram med länsvariabel och rätt tidsperiod ###########
df_ungrouped <-
  df %>%
  filter(!is.na(varinterest)) %>%
  prepare_df() %>%
  mutate(
    RegionName = region_namn,
    CountyOidExtension = lkf2CountyOidExtension(lkf),
    MeasurePeriodStart = date2MeasurePeriodStart(indikator_datum),
    MeasurePeriodEnd = date2MeasurePeriodEnd(indikator_datum)
  ) %>%
  group_by(MeasurePeriodStart, MeasurePeriodEnd) %>%
  filter(
    between_param_dates(indikator_datum),
    !is.na(CountyOidExtension),
    !is.na(MeasurePeriodStart),
    if (param$KON %in% 1:2) kon_value %in% param$KON else TRUE
  )

################## Skapa en ny dataram med gruppering på län ###################
df_lan <- group_by(df_ungrouped,  CountyOidExtension, add = TRUE )

################# Skapa en ny dataram med gruppering på region #################
df_reg <- group_by(df_ungrouped,  RegionName, add = TRUE )

#### Skapa funktion för att beräka ut kvotvärden samt första/sista mätvärde ####
summarise2 <- function(x) {
  x %>%
    summarise(
      Denominator           = sum(varinterest %in% 0:1),
      Numerator             = sum(varinterest == 1),
      FirstServiceEncounter = min(indikator_datum, na.rm = TRUE),
      LastServiceEncounter  = max(indikator_datum, na.rm = TRUE)
    )
}

################# Slå samman olika dataramar med olika stratum #################
df_all <- bind_rows(summarise2(df_ungrouped), summarise2(df_reg),summarise2(df_lan)) 

############ Skicka in den sammanslagna dataramen i VIS-funktionen #############
vis(df_all, minexp = 5)
