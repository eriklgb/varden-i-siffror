################################################################################
#                                                                              #
# Purpose:       Prostata-Väntetid_till_prostatacancerbesked                   #
#                                                                              #
# Author:        Christian Staf                                                #
# Contact:       Email: Christian.staf@rccvast.se                              #
# Client:        Christian Staf                                                #
#                                                                              #
# Code created:  2016-12-08                                                    #
# Last updated:  2016-12-08                                                    #
# Source:        /Users/christianstaf/Documents/Bitbucket/Repositories/varden  #
#                -i-siffror/Mallar/Prostata                                    #
#                                                                              #
# Comment:                                                                     #
#                                                                              #
################################################################################

########################## Hjälpfunktion för position ##########################
is.inca <- function() {
  env <-
    if (Sys.info()["nodename"] == "EXT-R27-PROD")
      "PROD"
  else if (Sys.info()["nodename"] == "EXT-R37-TEST")
    "TEST"
  else
    "LOCAL"
  return(env)
}

########## Lokala förberedelser samt inladdning av funktioner på INCA ########## 
if (is.inca() == "LOCAL") {
  library(rcc2)
  setwd("~/Documents/Bitbucket/Repositories/varden-i-siffror/Mallar/Prostata")
  if (!file.exists("df.rda")) {
    df <- read.csv2("df.txt")
    save(df, file = "df.rda")
  } else {
    load("df.rda")
  }
  param  <- list(start = c("2008-01-01", "2009-01-01", "2010-01-01", "2011-01-01", "2012-01-01","2013-01-01","2014-01-01", "2015-01-01"), 
                 slut = c("2008-12-31", "2009-12-31", "2010-12-31", "2011-12-31", "2012-12-31","2013-12-31","2014-12-31", "2015-12-31") ,
                 KON = 0)
}

################################ Ladda in paket ################################
library(dplyr)
library(incavis)



########################### Läs in Fredriks skript ###########################
# # # # # #
# Funktioner
# # # # # #

incaVarToDateYYMMDD <- 
  function(
    var=NULL
  ) {
    as.Date(as.character(var),format="%Y-%m-%d")
  }

# # # # # #
# # # # # #
# # # # # #

#
#
#
# Register: "Prostataregister"
# Vy: "Prostata_Nationell"
# Indikator: "Väntetid till prostatacancerbesked"
#
#
#

# Fixa variabelnamn och rensa bort Region Demo
# --------------------------------------------
colnames(df) <- tolower(colnames(df))
colnames(df) <- gsub("_värde","",colnames(df))

df <- subset(df,region_namn!="Region Demo")

# Behåll endast första förekomsten av personnummer
# ------------------------------------------------
df <- df[order(df$pat_id),]
df <- subset(df,!duplicated(data.frame(pat_id)))

# Definiera urvalsvariabler
# -------------------------
df$urval_period <- incaVarToDateYYMMDD(df$d_diadat)
df$urval_lakofo <- df$d_lkfdia
df$urval_sjukhus <- df$d_sjhkod

# Variabel av intresse (täljare om =1)
# ------------------------------------
df$d_datumpad <- incaVarToDateYYMMDD(df$d_datumpad)
df$b_datumpad <- incaVarToDateYYMMDD(df$b_datumpad)
df$d_datumpad[!is.na(df$b_datumpad)] <- df$b_datumpad[!is.na(df$b_datumpad)]
df$d_diadat <- incaVarToDateYYMMDD(df$d_diadat)

df$varinterest <- factor(1*((df$d_datumpad-df$d_diadat)<=11),levels=c(1,0))

# Specifikt urval för indikatorn (nämnare)
# ----------------------------------------
df <- 
  subset(
    df,
    !is.na(varinterest) & 
      (d_datumpad-d_diadat)>=0 & 
      !(!is.na(b_datumpadpatval) & b_datumpadpatval==1)
  )





########### Skapa en ny dataram med länsvariabel och rätt tidsperiod ###########
df_ungrouped <-
  df %>%
  prepare_df() %>%
  mutate(
    RegionName = region_namn,
    CountyOidExtension = lkf2CountyOidExtension(urval_lakofo),
    MeasurePeriodStart = date2MeasurePeriodStart(urval_period),
    MeasurePeriodEnd = date2MeasurePeriodEnd(urval_period)
  ) %>%
  group_by(MeasurePeriodStart, MeasurePeriodEnd) %>% 
  filter(
    between_param_dates(urval_period),
    !is.na(CountyOidExtension),
    !is.na(MeasurePeriodStart),
    if (param$KON %in% 1:2) kon_value %in% param$KON else TRUE
  )


################## Skapa en ny dataram med gruppering på län ###################
df_lan <- group_by(df_ungrouped,  CountyOidExtension, add = TRUE )

################# Skapa en ny dataram med gruppering på region #################
df_reg <- group_by(df_ungrouped,  RegionName, add = TRUE )

#### Skapa funktion för att beräka ut kvotvärden samt första/sista mätvärde ####
summarise2 <- function(x) {
  x %>%
    summarise(
      Denominator           = sum(varinterest %in% 0:1),
      Numerator             = sum(varinterest == 1),
      FirstServiceEncounter = min(urval_period, na.rm = TRUE),
      LastServiceEncounter  = max(urval_period, na.rm = TRUE)
    )
}


################# Slå samman olika dataramar med olika stratum #################
df_all <- bind_rows(summarise2(df_ungrouped), summarise2(df_lan), summarise2(df_reg)) 

############ Skicka in den sammanslagna dataramen i VIS-funktionen #############
vis(df_all, minexp = 5)





