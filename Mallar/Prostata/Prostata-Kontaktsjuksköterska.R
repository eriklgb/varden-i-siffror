################################################################################
#                                                                              #
# Purpose:       Prostata - Kontaktsjuksköterska                               #
#                                                                              #
# Author:        Christian Staf                                                #
# Contact:       Email: Christian.staf@rccvast.se                              #
# Client:        Christian Staf                                                #
#                                                                              #
# Code created:  2016-12-08                                                    #
# Last updated:  2016-12-08                                                    #
# Source:        /Users/christianstaf/Documents/Bitbucket/Repositories/varden  #
#                -i-siffror/Mallar/Prostata                                    #
#                                                                              #
# Comment:                                                                     #
#                                                                              #
################################################################################

########################## Hjälpfunktion för position ##########################
is.inca <- function() {
  env <-
    if (Sys.info()["nodename"] == "EXT-R27-PROD")
      "PROD"
  else if (Sys.info()["nodename"] == "EXT-R37-TEST")
    "TEST"
  else
    "LOCAL"
  return(env)
}

########## Lokala förberedelser samt inladdning av funktioner på INCA ########## 
if (is.inca() == "LOCAL") {
  library(rcc2)
  setwd("~/Documents/Bitbucket/Repositories/varden-i-siffror/Mallar/Prostata")
  if (!file.exists("df.rda")) {
    df <- read.csv2("df.txt")
    save(df, file = "df.rda")
  } else {
    load("df.rda")
  }
  param  <- list(start = c("2008-01-01", "2009-01-01", "2010-01-01", "2011-01-01", "2012-01-01","2013-01-01","2014-01-01", "2015-01-01"), 
                 slut = c("2008-12-31", "2009-12-31", "2010-12-31", "2011-12-31", "2012-12-31","2013-12-31","2014-12-31", "2015-12-31") ,
                 KON = 0)
}

################################ Ladda in paket ################################
library(dplyr)
library(incavis)


########################### Läs in Fredriks funktion ###########################
incaVarToDateYYMMDD <- 
  function(
    var=NULL
  ) {
    as.Date(as.character(var),format="%Y-%m-%d")
  }

npcrCalculateRiskgroup <- 
  function(
    data=NULL,
    groupLow=TRUE,
    groupHigh=TRUE
  ) {
    riskgroup <- rep(6,nrow(data))
    
    if (groupLow) {
      levelsLow <- 1
      labelsLow <- "Lågrisk"
    } else {
      levelsLow <- c(11,12,13)
      labelsLow <- c("Mycket låg risk","Lågrisk (övrig)","Lågrisk (saknas)")
    }
    
    levelsHigh <- c(31,32)
    labelsHigh <- c("Lokaliserad högrisk","Lokalt avancerad")
    if (groupHigh) {
      levelsHigh <- 3
      labelsHigh <- "Högrisk"
    }
    
    if (
      nrow(data)>0 & 
      all(
        c(
          "d_diadat",
          "rp_opanmdat",
          "s_stralanmdat",
          "d_tstad",
          "d_nstad",
          "d_mstad",
          "b_bildlymfnstadium",
          "b_bildskelmstadium",
          "rp_bildlymfnstadium",
          "rp_bildskelmstadium",
          "s_diagbildlymf",
          "s_diagbildlymfny",
          "s_diagbildfjarr",
          "d_spsa",
          "d_gleasett",
          "d_gleastva",
          "d_gleassa",
          "d_diffgrad",
          "d_biop",
          "d_biopca",
          "d_mmcancer",
          "d_vol"
        )%in%colnames(data)
      )
    ) {
      t <- data$d_tstad
      n <- rep(NA,nrow(data))
      m <- rep(NA,nrow(data))
      
      data$d_diadat <- incaVarToDateYYMMDD(data$d_diadat)
      data$rp_opanmdat <- incaVarToDateYYMMDD(data$rp_opanmdat)
      data$s_stralanmdat <- incaVarToDateYYMMDD(data$s_stralanmdat)
      
      getImagingFromRP <- !is.na(data$d_diadat) & !is.na(data$rp_opanmdat) & (data$rp_opanmdat-data$d_diadat)<=365.24
      getImagingFromS <- !is.na(data$d_diadat) & !is.na(data$s_stralanmdat) & (data$s_stralanmdat-data$d_diadat)<=365.24
      
      selectionReplaceOld <- !(!is.na(data$s_diagbildlymf) & data$s_diagbildlymf%in%0:1)
      data$s_diagbildlymf[selectionReplaceOld] <- data$s_diagbildlymfny[selectionReplaceOld]
      
      # N-stadium
      n[is.na(n) & (
        (!is.na(data$b_bildlymfnstadium) & data$b_bildlymfnstadium==1) | 
          (getImagingFromRP & !is.na(data$rp_bildlymfnstadium) & data$rp_bildlymfnstadium==1) | 
          (getImagingFromS & !is.na(data$s_diagbildlymf) & data$s_diagbildlymf==1)
      )] <- 1
      n[is.na(n) & (
        (!is.na(data$b_bildlymfnstadium) & data$b_bildlymfnstadium==0) | 
          (getImagingFromRP & !is.na(data$rp_bildlymfnstadium) & data$rp_bildlymfnstadium==0) | 
          (getImagingFromS & !is.na(data$s_diagbildlymf) & data$s_diagbildlymf==0)
      )] <- 0
      n[is.na(n) & !is.na(data$d_nstad) & data$d_nstad==1] <- 1
      n[is.na(n) & !is.na(data$d_nstad) & data$d_nstad==0] <- 0
      
      # M-stadium
      m[is.na(m) & (
        (!is.na(data$b_bildskelmstadium) & data$b_bildskelmstadium==1) | 
          (getImagingFromRP & !is.na(data$rp_bildskelmstadium) & data$rp_bildskelmstadium==1) | 
          (getImagingFromS & !is.na(data$s_diagbildfjarr) & data$s_diagbildfjarr==1)
      )] <- 1
      m[is.na(m) & (
        (!is.na(data$b_bildskelmstadium) & data$b_bildskelmstadium==0) | 
          (getImagingFromRP & !is.na(data$rp_bildskelmstadium) & data$rp_bildskelmstadium==0) | 
          (getImagingFromS & !is.na(data$s_diagbildfjarr) & data$s_diagbildfjarr==0)
      )] <- 0
      m[is.na(m) & !is.na(data$d_mstad) & data$d_mstad==1] <- 1
      m[is.na(m) & !is.na(data$d_mstad) & data$d_mstad==0] <- 0
      
      psa <- data$d_spsa
      
      # För vissa äldre data har Gleasonsumman inte räknats ut automatiskt från Gleason 1 och Gleason 2
      calculateGleasonSum <- !is.na(data$d_gleasett) & data$d_gleasett>=1 & data$d_gleasett<=5 & !is.na(data$d_gleastva) & data$d_gleastva>=1 & data$d_gleastva<=5
      data$d_gleassa[calculateGleasonSum] <- data$d_gleasett[calculateGleasonSum] + data$d_gleastva[calculateGleasonSum]
      
      gleassa <- data$d_gleassa
      
      diffgrad <- data$d_diffgrad
      
      # Riskgrupp
      riskgroup[
        riskgroup==6 & 
          ( 
            (!is.na(psa) & psa>=100) | 
              (!is.na(m) & m==1) 
          )
        ] <- 5
      
      riskgroup[
        riskgroup==6 & 
          ( 
            (!is.na(psa) & psa>=50) | 
              (!is.na(n) & n==1) | 
              (!is.na(t) & t==4) 
          )
        ] <- 4
      
      riskgroup[
        riskgroup==6 & 
          ( 
            !is.na(t) & t==3
          )
        ] <- 32
      
      riskgroup[
        riskgroup==6 & 
          ( 
            (!is.na(gleassa) & gleassa%in%8:10) | 
              (is.na(gleassa) & !is.na(diffgrad) & diffgrad==3) | 
              (!is.na(psa) & psa>=20) 
          )
        ] <- 31
      
      riskgroup[
        riskgroup==6 & 
          ( 
            (!is.na(t) & t%in%c(0,1,11,12,13,2)) &
              (
                (!is.na(gleassa) & gleassa%in%7) | 
                  (is.na(gleassa) & !is.na(diffgrad) & diffgrad==2) | 
                  (!is.na(psa) & psa>=10)
              )
          )
        ] <- 2
      
      riskgroup[
        riskgroup==6 & 
          ( 
            (!is.na(t) & t%in%c(0,1,11,12,13,2)) & 
              (!is.na(psa) & psa<10) & 
              (
                (!is.na(gleassa) & gleassa%in%2:6) | 
                  (is.na(gleassa) & !is.na(diffgrad) & diffgrad==1)
              ) 
          )
        ] <- 1
      
      if (!groupLow) {
        biop <- data$d_biop
        biopca <- data$d_biopca
        mmcancer <- data$d_mmcancer
        
        data$d_vol[!is.na(data$d_vol) & data$d_vol<=0] <- NA
        vol <- data$d_vol
        
        riskgroup[
          riskgroup==1 & 
            !is.na(t) & t==13 & 
            !is.na(biopca) & biopca>=0 & biopca<=4 & 
            !is.na(biop) & biop>=8 & 
            !is.na(mmcancer) & mmcancer>=0 & mmcancer<8 & 
            !is.na(psa/vol) & (psa/vol)>=0 & (psa/vol)<0.15
          ] <- 11
        
        riskgroup[
          riskgroup==1 & 
            (
              (!is.na(t) & t!=13 ) | 
                (!is.na(biopca) & biopca>4) | 
                (!is.na(biop) & biop>=0 & biop<8) | 
                (!is.na(mmcancer) & mmcancer>=8) | 
                (!is.na(psa/vol) & psa/vol>=0.15)
            )
          ] <- 12
        
        riskgroup[riskgroup==1] <- 13
      }
      
      if (groupHigh) {
        riskgroup[riskgroup%in%c(3,31,32)] <- 3
      }
    }
    
    riskgroup <- 
      factor(
        riskgroup,
        levels=c(levelsLow,2,levelsHigh,4,5,6),
        labels=c(labelsLow,"Mellanrisk",labelsHigh,"Regionalt metastaserad","Fjärrmetastaserad","Uppgift saknas")
      )
    
    riskgroup
  }

npcrCalculatePrimaryTreatment <- 
  function(
    data=NULL
  ) {
    primary_treatment <- rep(999,nrow(data))
    
    if (
      nrow(data)>0 & 
      all(
        c(
          "b_avlinnbeh",
          "b_behtyp",
          "b_konster",
          "b_kurativterapi",
          "b_prostekt",
          "b_prostektdat",
          "b_operationstyp",
          "b_opremdat",
          "b_rt",
          "b_rtremdat",
          "rp_opej",
          "rp_opdat",
          "rp_tidannprimbeh",
          "rp_optyp",
          "s_stralej",
          "s_startdatboost",
          "s_startdatrt",
          "s_startdatseeds",
          "s_tidannprimbeh",
          "s_postop_rt",
          "s_primextrt",
          "s_boost",
          "s_boostspec",
          "s_seeds",
          "s_orebromod02"
        )%in%colnames(data)
      )
    ) {
      
      data$b_opremdat <- incaVarToDateYYMMDD(data$b_opremdat)
      data$b_rtremdat <- incaVarToDateYYMMDD(data$b_rtremdat)
      
      data$b0_rp_planned <- 0
      data$b0_rp_planned[
        with(
          data,
          (!is.na(b_kurativterapi) & b_kurativterapi==1) |
            (!is.na(b_prostekt) & b_prostekt%in%c(1,2,3,9)) | 
            !is.na(b_opremdat)
        )
        ] <- 1
      
      data$b0_rt_planned <- 0
      data$b0_rt_planned[
        with(
          data,
          (!is.na(data$b_kurativterapi) & data$b_kurativterapi==2) |
            (!is.na(data$b_rt) & data$b_rt%in%c(1,2,3,9)) | 
            !is.na(data$b_rtremdat)
        )
        ] <- 1
      
      # RP
      data$rp0_date <- incaVarToDateYYMMDD(data$rp_opdat)
      data$rp0_date[is.na(data$rp0_date)] <- incaVarToDateYYMMDD(data$b_prostektdat)[is.na(data$rp0_date)]
      
      selectionRP <- with(data,!(!is.na(rp_opej) & rp_opej==1) & (b0_rp_planned==1 | !is.na(rp0_date)))
      data$rp0_primary <- NA
      data$rp0_primary[selectionRP] <- 0
      data$rp0_primary[
        selectionRP & 
          with(
            data,
            !(!is.na(b_behtyp) & b_behtyp%in%c(1,3)) & 
              (
                (!is.na(rp_tidannprimbeh) & rp_tidannprimbeh==0) |
                  ((is.na(rp_tidannprimbeh) | rp_tidannprimbeh==98) & b0_rp_planned==1)
              )
          )
        ] <- 1
      
      data$rp0_type <- data$rp_optyp
      data$rp0_type[is.na(data$rp0_type)] <- data$b_operationstyp[is.na(data$rp0_type)]
      data$rp0_type[is.na(data$rp0_type)] <- data$b_prostekt[is.na(data$rp0_type)]
      data$rp0_type[is.na(data$rp0_type) & selectionRP] <- 9
      
      data$rp0_type[!selectionRP] <- NA
      
      # RT
      data$s0_date <- pmin(
        incaVarToDateYYMMDD(data$s_startdatboost),
        incaVarToDateYYMMDD(data$s_startdatrt),
        incaVarToDateYYMMDD(data$s_startdatseeds),
        na.rm=TRUE
      )
      
      selectionRT <- with(data,!(!is.na(s_stralej) & s_stralej==1) & (b0_rt_planned==1 | !is.na(s0_date)))
      data$s0_primary <- NA
      data$s0_primary[selectionRT] <- 0
      data$s0_primary[
        selectionRT & 
          with(
            data,
            !(!is.na(b_behtyp) & b_behtyp%in%c(1,3)) & 
              (
                (!is.na(s_tidannprimbeh) & s_tidannprimbeh==0) |
                  ((is.na(s_tidannprimbeh) | s_tidannprimbeh==98) & b0_rt_planned==1)
              )
          )
        ] <- 1
      
      selectionPostopRT <- !(!is.na(data$s_stralej) & data$s_stralej==1) & !is.na(data$s_postop_rt) & data$s_postop_rt%in%1:3
      data$s0_primary[selectionPostopRT] <- 0
      
      data$s0_type <- NA
      
      data$s_boost[!is.na(data$s_boostspec) & data$s_boostspec%in%1:3] <- 1
      
      data$s0_type[
        with(data,
             is.na(s0_type) & 
               !is.na(s_primextrt) & s_primextrt==1 & 
               !(!is.na(s_boost) & s_boost==1)
        )
        ] <- 1
      
      data$s0_type[
        with(data,
             is.na(data$s0_type) & 
               !is.na(s_primextrt) & s_primextrt==1 & 
               !is.na(s_boost) & s_boost==1 & 
               !is.na(s_boostspec) & s_boostspec==1
        )
        ] <- 3
      
      data$s0_type[
        with(data,
             is.na(s0_type) & 
               !is.na(s_primextrt) & s_primextrt==1 & 
               !is.na(s_boost) & s_boost==1 & 
               !is.na(s_boostspec) & s_boostspec%in%2:4
        )
        ] <- 1
      
      data$s0_type[
        with(data,
             is.na(s0_type) & 
               !(!is.na(s_primextrt) & s_primextrt==1) & 
               (
                 (
                   !is.na(s_boost) & s_boost==1 & 
                     !is.na(s_boostspec) & s_boostspec==1
                 ) | 
                   !is.na(s_seeds) & s_seeds==1
               )
        )
        ] <- 2
      
      data$s0_type[
        with(data,
             is.na(s0_type) & 
               !(!is.na(s_primextrt) & s_primextrt==1) & 
               !is.na(s_orebromod02) & s_orebromod02==1 & 
               !is.na(s_boostspec) & s_boostspec==1
        )
        ] <- 2
      
      data$s0_type[is.na(data$s0_type)] <- data$b_rt[is.na(data$s0_type)]
      
      data$s0_type[!selectionRT | selectionPostopRT] <- NA
      
      # Primärbehandling
      primary_treatment[primary_treatment==999 & data$rp0_primary==1] <- 219
      primary_treatment[primary_treatment==219 & !is.na(data$rp0_type) & data$rp0_type==1] <- 211
      primary_treatment[primary_treatment==219 & !is.na(data$rp0_type) & data$rp0_type==2] <- 212
      primary_treatment[primary_treatment==219 & !is.na(data$rp0_type) & data$rp0_type==3] <- 213
      primary_treatment[primary_treatment==999 & data$s0_primary==1] <- 229
      primary_treatment[primary_treatment==229 & !is.na(data$s0_type) & data$s0_type==1] <- 221
      primary_treatment[primary_treatment==229 & !is.na(data$s0_type) & data$s0_type==2] <- 222
      primary_treatment[primary_treatment==229 & !is.na(data$s0_type) & data$s0_type==3] <- 223
      primary_treatment[primary_treatment==999 & !is.na(data$b_behtyp) & data$b_behtyp==1] <- 109
      primary_treatment[primary_treatment==109 & !is.na(data$b_konster) & data$b_konster==1] <- 101
      primary_treatment[primary_treatment==109 & !is.na(data$b_konster) & data$b_konster==2] <- 102
      primary_treatment[primary_treatment==999 & data$rp0_primary==0 & !is.na(data$rp_tidannprimbeh) & data$rp_tidannprimbeh==1] <- 101
      primary_treatment[primary_treatment==999 & data$s0_primary==0 & !is.na(data$s_tidannprimbeh) & data$s_tidannprimbeh==1] <- 101
      primary_treatment[primary_treatment==999 & data$rp0_primary==0 & !is.na(data$rp_tidannprimbeh) & data$rp_tidannprimbeh==2] <- 102
      primary_treatment[primary_treatment==999 & data$s0_primary==0 & !is.na(data$s_tidannprimbeh) & data$s_tidannprimbeh==2] <- 102
      primary_treatment[primary_treatment==999 & data$rp0_primary==0 & !is.na(data$rp_tidannprimbeh) & data$rp_tidannprimbeh==3] <- 229
      primary_treatment[primary_treatment==999 & data$s0_primary==0 & !is.na(data$s_tidannprimbeh) & data$s_tidannprimbeh==3] <- 229
      primary_treatment[primary_treatment==999 & data$rp0_primary==0 & !is.na(data$rp_tidannprimbeh) & data$rp_tidannprimbeh==4] <- 399
      primary_treatment[primary_treatment==999 & data$s0_primary==0 & !is.na(data$s_tidannprimbeh) & data$s_tidannprimbeh==4] <- 399
      primary_treatment[primary_treatment==999 & is.na(data$rp0_primary) & data$b0_rp_planned==1 & !(!is.na(data$rp_opej) & data$rp_opej==1)] <- 219
      primary_treatment[primary_treatment==999 & is.na(data$s0_primary) & data$b0_rt_planned==1 & !(!is.na(data$s_stralej) & data$s_stralej==1)] <- 229
      primary_treatment[primary_treatment==999 & !is.na(data$b_behtyp) & data$b_behtyp==2] <- 299
      primary_treatment[primary_treatment==999 & !is.na(data$b_behtyp) & data$b_behtyp==3] <- 399
      primary_treatment[primary_treatment==999 & ((!is.na(data$b_behtyp) & data$b_behtyp==4) |(!is.na(data$b_avlinnbeh) & data$b_avlinnbeh==1))] <- 409
    }
    
    primary_treatment <- 
      factor(
        primary_treatment,
        levels=c(
          101,
          102,
          109,
          211,
          212,
          213,
          219,
          221,
          222,
          223,
          229,
          299,
          399,
          409,
          999
        ),
        labels=c(
          "10a active surveillance",
          "10b watchful waiting",
          "10x conservative, type missing",
          "21a rp, retropubic",
          "21b rp, laparoscopic",
          "21c rp, robot assisted",
          "21x rp, type missing",
          "22a rt, external",
          "22b rt, brachy",
          "22c rt, external + brachy",
          "22x rt, type missing",
          "29x curative, type missing",
          "39x non-curative",
          "40x dead before treatment decision",
          "99x missing"
        )
      )
    
    primary_treatment
  }

# # # # # #
# # # # # #
# # # # # #

#
#
#
# Register: "Prostataregister"
# Vy: "Prostata_Nationell"
# Indikator: "Kontaktsjuksköterska vid nydiagnosticerad prostatacancer"
#
#
#

# Fixa variabelnamn och rensa bort Region Demo
# --------------------------------------------
colnames(df) <- tolower(colnames(df))
colnames(df) <- gsub("_värde","",colnames(df))

df <- subset(df,region_namn!="Region Demo")

# Behåll endast första förekomsten av personnummer
# ------------------------------------------------
df <- df[order(df$pat_id),]
df <- subset(df,!duplicated(data.frame(pat_id)))

# Definiera urvalsvariabler
# -------------------------
df$urval_period <- incaVarToDateYYMMDD(df$d_diadat)
df$urval_lakofo <- df$d_lkfdia

replaceCode <- !is.na(df$b_behbesremsjhkod)
df$b_sjhkod[replaceCode] <- df$b_behbesremsjhkod[replaceCode]
df$urval_sjukhus <- df$b_sjhkod

# Beräkna riskgrupp
# -----------------
df$stggrp <- npcrCalculateRiskgroup(data=df)

# Beräkna primärbehandling
# ------------------------
df$newbehgrp <- npcrCalculatePrimaryTreatment(data=df)

# Variabel av intresse (täljare om =1)
# ------------------------------------
df$d_kontsjukskoterska[is.na(df$d_kontsjukskoterska)] <- df$d_kontsjukskoterskany[is.na(df$d_kontsjukskoterska)]
df$b_kontsjukskoterska[is.na(df$b_kontsjukskoterska)] <- df$b_kontsjukskoterskany[is.na(df$b_kontsjukskoterska)]

df$fs_kontaktssk <- rep(NA,nrow(df))
df$fs_kontaktssk[is.na(df$fs_kontaktssk) & ((!is.na(df$d_kontsjukskoterska) & df$d_kontsjukskoterska==1) | (!is.na(df$b_kontsjukskoterska) & df$b_kontsjukskoterska==1) | (substr(df$newbehgrp,1,2)=="22" & !is.na(df$s_kontsjukskoterska) & df$s_kontsjukskoterska==1) | (substr(df$newbehgrp,1,2)=="21" & !is.na(df$rp_kontsjukskoterska) & df$rp_kontsjukskoterska==1))] <- 1
df$fs_kontaktssk[is.na(df$fs_kontaktssk) & ((!is.na(df$d_kontsjukskoterska) & df$d_kontsjukskoterska==0) | (!is.na(df$b_kontsjukskoterska) & df$b_kontsjukskoterska==0) | (substr(df$newbehgrp,1,2)=="22" & !is.na(df$s_kontsjukskoterska) & df$s_kontsjukskoterska==0) | (substr(df$newbehgrp,1,2)=="21" & !is.na(df$rp_kontsjukskoterska) & df$rp_kontsjukskoterska==0))] <- 0

df$varinterest <- factor(df$fs_kontaktssk,levels=c(1,0))

# Specifikt urval för indikatorn (nämnare)
# ----------------------------------------
df <- 
  subset(
    df,
    !is.na(varinterest)
  )



########### Skapa en ny dataram med länsvariabel och rätt tidsperiod ###########
df_ungrouped <-
  df %>%
  prepare_df() %>%
  mutate(
    RegionName = region_namn,
    CountyOidExtension = lkf2CountyOidExtension(urval_lakofo),
    MeasurePeriodStart = date2MeasurePeriodStart(urval_period),
    MeasurePeriodEnd = date2MeasurePeriodEnd(urval_period)
  ) %>%
  group_by(MeasurePeriodStart, MeasurePeriodEnd) %>% 
  filter(
    between_param_dates(urval_period),
    !is.na(CountyOidExtension),
    !is.na(MeasurePeriodStart),
    if (param$KON %in% 1:2) kon_value %in% param$KON else TRUE
  )



################## Skapa en ny dataram med gruppering på län ###################
df_lan <- group_by(df_ungrouped,  CountyOidExtension, add = TRUE )

################# Skapa en ny dataram med gruppering på region #################
df_reg <- group_by(df_ungrouped,  RegionName, add = TRUE )

#### Skapa funktion för att beräka ut kvotvärden samt första/sista mätvärde ####
summarise2 <- function(x) {
    x %>%
        summarise(
            Denominator           = sum(varinterest %in% 0:1),
            Numerator             = sum(varinterest == 1),
            FirstServiceEncounter = min(urval_period, na.rm = TRUE),
            LastServiceEncounter  = max(urval_period, na.rm = TRUE)
        )
}


################# Slå samman olika dataramar med olika stratum #################
df_all <- bind_rows(summarise2(df_ungrouped), summarise2(df_lan), summarise2(df_reg)) 



############ Skicka in den sammanslagna dataramen i VIS-funktionen #############
vis(df_all, minexp = 5)





