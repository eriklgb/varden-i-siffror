################################################################################
#                                                                              #
# Purpose:       Kolon - Preoperativ MDT-konferens                             #
#                                                                              #
# Author:        Christian Staf                                                #
# Contact:       Email: Christian.staf@rccvast.se                              #
# Client:        Christian Staf                                                #
#                                                                              #
# Code created:  2017-02-27                                                    #
# Last updated:  2017-02-27                                                    #
# Source:        /Users/christianstaf/Documents/Bitbucket/Repositories/varden  #
#                -i-siffror/Mallar/Kolorektal                                  #
#                                                                              #
# Comment:                                                                     #
#                                                                              #
################################################################################

########################## Hjälpfunktion för position ##########################
is.inca <- function() {
  env <-
    if (Sys.info()["nodename"] == "EXT-R27-PROD")
      "PROD"
  else if (Sys.info()["nodename"] == "EXT-R37-TEST")
    "TEST"
  else
    "LOCAL"
  return(env)
}

########## Lokala förberedelser samt inladdning av funktioner på INCA ########## 
if (is.inca() == "LOCAL") {
  setwd("~/Documents/Bitbucket/Repositories/varden-i-siffror/Mallar/Kolorektal")
  if (!file.exists("df.rda")) {
    df <- read.csv2("df.txt")
    save(df, file = "df.rda")
  } else {
    load("df.rda")
  }
  param  <- list(start = c("2008-01-01", "2009-01-01", "2010-01-01", "2011-01-01", "2012-01-01","2013-01-01","2014-01-01", "2015-01-01","2016-01-01"), 
                 slut = c("2008-12-31", "2009-12-31", "2010-12-31", "2011-12-31", "2012-12-31","2013-12-31","2014-12-31", "2015-12-31","2016-12-31") ,
                 KON = 0)
}

# Namn på register:  Svenska Kolorektalcancerregistret
# Namn på indikator: Preoperativ MDT-konferens
# Namn på vy indikator skall basras på: Exempelvy
################################ Ladda in paket ################################
library(dplyr)
library(incavis)

################################################################################
#                                                                              #
#                   Beräkning av urvalsramen för indikatorn                    #
#                                                                              #
################################################################################
names(df)<-tolower(names(df))

df <- subset(df,
             (a2_tumlok_värde%in%1 & 
                a3_adeno_värde%in%c(1,NA) &
                !is.na(r225t721_id) &
                !duplicated(df[c("pat_id","r225t721_id")]) &     
                !(a2_atgard_beskrivning %in% 'Endoskopisk polypektomi (som enda åtgärd)') &
                !(a2_optyp1_beskrivning %in% 'Akut'))) 

################################################################################
#                                                                              #
#                        Skapa variabler med mätvärden                         #
#                                                                              #
################################################################################

##################################### Kvot #####################################
df$varinterest <- with(df,ifelse(a2_prebed_värde%in% 1, 1, 0))
df$indikator_datum <- df$a1_diagnosdatum           
df$lkf <- df$a0_lkfdia
df$sjukhus_kod <- ifelse(!is.na(df$a2_sjhkod), df$a2_sjhkod, df$a0_opsjhkod)



########### Skapa en ny dataram med länsvariabel och rätt tidsperiod ###########
df_ungrouped <-
  df %>%
  filter(!is.na(varinterest)) %>%
  prepare_df() %>%
  mutate(
    RegionName = region_namn,
    CountyOidExtension = lkf2CountyOidExtension(lkf),
    MeasurePeriodStart = date2MeasurePeriodStart(indikator_datum),
    MeasurePeriodEnd = date2MeasurePeriodEnd(indikator_datum)
  ) %>%
  group_by(MeasurePeriodStart, MeasurePeriodEnd) %>%
  filter(
    between_param_dates(indikator_datum),
    !is.na(CountyOidExtension),
    !is.na(MeasurePeriodStart),
    if (param$KON %in% 1:2) kon_value %in% param$KON else TRUE
  )

################## Skapa en ny dataram med gruppering på län ###################
df_lan <- group_by(df_ungrouped,  CountyOidExtension, add = TRUE )

################# Skapa en ny dataram med gruppering på region #################
df_reg <- group_by(df_ungrouped,  RegionName, add = TRUE )

#### Skapa funktion för att beräka ut kvotvärden samt första/sista mätvärde ####
summarise2 <- function(x) {
  x %>%
    summarise(
      Denominator           = sum(varinterest %in% 0:1),
      Numerator             = sum(varinterest == 1),
      FirstServiceEncounter = min(indikator_datum, na.rm = TRUE),
      LastServiceEncounter  = max(indikator_datum, na.rm = TRUE)
    )
}

################# Slå samman olika dataramar med olika stratum #################
df_all <- bind_rows(summarise2(df_ungrouped), summarise2(df_reg),summarise2(df_lan)) 

############ Skicka in den sammanslagna dataramen i VIS-funktionen #############
vis(df_all, minexp = 5)
