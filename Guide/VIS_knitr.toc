\select@language {swedish}
\contentsline {section}{\numberline {1}Information om dokumentet}{4}{section.1}
\contentsline {section}{\numberline {2}Involverade parter}{4}{section.2}
\contentsline {section}{\numberline {3}Arbetsg\IeC {\r a}ng}{4}{section.3}
\contentsline {section}{\numberline {4}R-paketet 'incavis'}{6}{section.4}
\contentsline {subsection}{\numberline {4.1}Bakgrund}{6}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Paketets syfte}{6}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Paketets begr\IeC {\"a}nsningar}{6}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Utveckling}{6}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Installation}{6}{subsection.4.5}
\contentsline {subsubsection}{\numberline {4.5.1}Lokal installation}{7}{subsubsection.4.5.1}
\contentsline {subsubsection}{\numberline {4.5.2}Installation p\IeC {\r a} INCA}{7}{subsubsection.4.5.2}
\contentsline {subsection}{\numberline {4.6}Data och parameterv\IeC {\"a}rden}{8}{subsection.4.6}
\contentsline {subsection}{\numberline {4.7}Skapa en exempeldatafil}{8}{subsection.4.7}
\contentsline {subsection}{\numberline {4.8}Exempel p\IeC {\r a} anv\IeC {\"a}ndadet av st\IeC {\"o}d- och huvudfunktionen}{8}{subsection.4.8}
\contentsline {subsubsection}{\numberline {4.8.1}Data och parametrar}{8}{subsubsection.4.8.1}
\contentsline {subsubsection}{\numberline {4.8.2}Datatransformation}{10}{subsubsection.4.8.2}
\contentsline {subsubsection}{\numberline {4.8.3}Exportert av data}{12}{subsubsection.4.8.3}
\contentsline {subsubsection}{\numberline {4.8.4}Kontroll av data}{12}{subsubsection.4.8.4}
\contentsline {subsection}{\numberline {4.9}Ny huvudfunktion som \IeC {\"a}ven utf\IeC {\"o}r f\IeC {\"o}rberedande bearbetning}{12}{subsection.4.9}
\contentsline {section}{\numberline {5}Skapa mall p\IeC {\r a} INCA}{15}{section.5}
\contentsline {subsection}{\numberline {5.1}Bakgrund}{15}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Parametrar i mallen}{15}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Urval}{16}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}R-bearbetningen}{16}{subsection.5.4}
\contentsline {section}{\numberline {6}Kvalitetsindikatorer}{19}{section.6}
\contentsline {section}{\numberline {7}Exempel}{19}{section.7}
\contentsline {subsection}{\numberline {7.1}Skapa mallen}{19}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Skapa kvalitetsindikator}{21}{subsection.7.2}
