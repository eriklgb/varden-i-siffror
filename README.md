![alt text](http://www.cancercentrum.se/globalassets/bilder/vara-uppdrag/kunskapsstyrning/kvalitetsregister/inlogg-inca-bildpuff_300px.jpg "Logo Title Text 1")
> #Vården i siffror (VIS)#
>
> Skapare: Christian Staf
>
> Datum: 6 oktober 2016


***
## Vad är detta repository till för? ##
Syftet med detta repository är för att förenkla versionshanteringen av arbetet med VIS.

***
## Förklaring av mappstrukturen på repository ##
Det finns tre mappar i detta repository. Dessa är 'Godkännande av indikatorer', ' Lathund för bearbetning för nationella statistiker' och 'Mallar'.

* **Godkännande av indikatorer:** Denna mapp innehåller R-skriptet för att skapa ett html-objekt innehållande en highcharts för en indikator. Detta R-skript använder paketet 'Rcharts' för att skapa en highcharts oberoende av separat jquery och javascript bibliotek. I skriptet specificeras filnamnet på filen med den data i det format INCA:s indikatordefinition (se INCA-hjälpen för mer information) förväntar sig. 

* **Lathund för R-bearbetning för nationella statistiker: ** I denna mapp finns  ett R-skript med en beskrivning hur nationella statistiker kan specificera en indikator i R-skript.

* **Mallar:** I denna mapp finns R-skript för att bearbeta fram indikatorerna i det önskade formatet. Skriptet är det som används i mallen på INCA. Skriptet kan även köras lokalt men då förutsätts att paketet 'incavis' (skapat av Erik Bülow) samt 'dplyr' finns installerat. Paketet 'incavis' kan hämtas från Bitbucket med devtools paketet (devtools::install_bitbucket("cancercentrum/invavis") medan 'dplyr' finns på cran.

***
## Mer dokumentation kommer vid ett senare tillfälle ... ##