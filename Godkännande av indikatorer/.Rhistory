FS_FormatDate <- function(f_var=NULL) {
return(as.Date(as.character(f_var),format="%Y-%m-%d"))
}
FS_PrepareDiaDtm <- function(f_data=df) {
f_data$a_provdtm <- FS_FormatDate(f_data$a_provdtm)
f_data$a_visadtm <- FS_FormatDate(f_data$a_visadtm)
f_diadtm <- f_data$a_provdtm
f_diadtm[is.na(f_diadtm)] <- f_data$a_visadtm[is.na(f_diadtm)]
return(f_diadtm)
}
FS_PrepareStage <- function(f_data=df,groupAB=FALSE) {
f_stage <- as.character(f_data$a_stadium)
f_stage[is.na(f_stage) | f_stage==""] <- "Uppgift saknas"
if(groupAB) {
levels1 <- "I"
levels2 <- "II"
levels3 <- "III"
} else {
levels1 <- c("IA","IB")
levels2 <- c("IIA","IIB")
levels3 <- c("IIIA","IIIB")
}
f_stage <- factor(f_stage,levels=c(levels1,levels2,levels3,"IV","Uppgift saknas"))
return(f_stage)
}
FS_PrepareDiagnos <- function(f_data=df,group=TRUE) {
f_data$a_diagnos[is.na(f_data$a_diagnos)] <- 99
if(group) {
f_diagnosgrp <- rep(99,nrow(f_data))
f_diagnosgrp[f_data$a_diagnos%in%c(1,3,4,5,6,9)] <- 1
f_diagnosgrp[f_data$a_diagnos%in%c(2)] <- 2
f_diagnosgrp[f_data$a_diagnos%in%c(7,8,91)] <- 3
f_diagnosgrp <- factor(f_diagnosgrp,levels=c(1:3,99),labels=c("NSCLC","SCLC","Övriga","Uppgift saknas"))
} else {
f_diagnosgrp <- f_data$a_diagnos
f_diagnosgrp[is.na(f_diagnosgrp)] <- 99
f_diagnosgrp <- factor(f_diagnosgrp,levels=c(1:9,91,99),labels=c("Skivepitel","Småcellig","Adenocarcinom","Storcellig/lågt diff. icke-småcellig","Adenoskvamös","Pleomorfa/sarkomatösa inslag","Carcinoid","Spottkörteltyp","Oklassificerad cancer","Cytologisk/histologisk diagnos föreligger ej","Uppgift saknas"))
}
return(f_diagnosgrp)
}
################################################################################
#                                                                              #
#                   Beräkning av urvalsramen för indikatorn                    #
#                                                                              #
################################################################################
# Fixa variabelnamn och rensa bort Region Demo
# --------------------------------------------
colnames(df) <- tolower(colnames(df))
colnames(df) <- gsub("_värde","",colnames(df))
df <- subset(df,region_namn!="Region Demo")
# Behåll endast första kombinationen av personnummer och löpnummer
# ----------------------------------------------------------------
df <- df[order(df$pat_id,df$lopnr),]
df <- subset(df,!duplicated(data.frame(pat_id,lopnr)))
# Defniera variabler
# ------------------
df$a_diadtm <- FS_PrepareDiaDtm()
df$a_diagnosgrp <- FS_PrepareDiagnos()
df$a_stagegrp <- FS_PrepareStage()
# Definiera urvalsvariabler
# -------------------------
df$urval_period <- df$a_diadtm
df$urval_lakofo <- df$a_lkfaig
df$urval_sjukhus <- df$a_sjukhus
# Variabel av intresse (täljare om =1)
# ------------------------------------
df$varinterest <- factor(1*(df$a_grundpet==1),levels=c(1,0))
# Specifikt urval för indikatorn (nämnare)
# ----------------------------------------
df  <- subset(df,!is.na(varinterest) & a_diagnosgrp=="NSCLC" & a_stagegrp %in% c("IB","IIA","IIB","IIIA","IIIB") & ((!is.na(a_kirurgi) & a_kirurgi==1) | (!is.na(a_prikemo) & a_prikemo==1)| (!is.na(a_stestral) & a_stestral==1)))
########### Skapa en ny dataram med länsvariabel och rätt tidsperiod ###########
df_ungrouped <-
df %>%
prepare_df() %>%
mutate(
RegionName = region_namn,
CountyOidExtension = lkf2CountyOidExtension(urval_lakofo),
MeasurePeriodStart = date2MeasurePeriodStart(urval_period),
MeasurePeriodEnd = date2MeasurePeriodEnd(urval_period)
) %>%
group_by(MeasurePeriodStart, MeasurePeriodEnd) %>%
filter(
between_param_dates(urval_period),
!is.na(CountyOidExtension),
!is.na(MeasurePeriodStart),
if (param$KON %in% 1:2) kon_value %in% param$KON else TRUE
)
################## Skapa en ny dataram med gruppering på län ###################
df_lan <- group_by(df_ungrouped,  CountyOidExtension, add = TRUE )
#### Skapa funktion för att beräka ut kvotvärden samt första/sista mätvärde ####
summarise2 <- function(x) {
x %>%
summarise(
Denominator           = sum(varinterest %in% 0:1),
Numerator             = sum(varinterest == 1),
FirstServiceEncounter = min(urval_period, na.rm = TRUE),
LastServiceEncounter  = max(urval_period, na.rm = TRUE)
)
}
################# Slå samman olika dataramar med olika stratum #################
df_all <- bind_rows(summarise2(df_ungrouped), summarise2(df_lan))
############ Skicka in den sammanslagna dataramen i VIS-funktionen #############
vis(df_all, minexp = 5)
################################################################################
#                                                                              #
#            Skapande av inbäddad HTML fil för presentation av VIS             #
#                                                                              #
################################################################################
filnamn <- "Lunga - VIS - PET-DT"
titel <- filnamn
# filnamn <- "Prostata - VIS - Kontaktsjuksköterska"
# titel <- "Prostata - VIS - Kontaktsjuksköterska"
############################# Förberedande arbete ##############################
library(rcc)
library(rCharts)
library(dplyr)
library(jsonlite)
library(tidyr)
setwd("~/Documents/Bitbucket/Repositories/varden-i-siffror/Godkännande av indikatorer")
######## Ladda in fil och gör gemensamma bearbetningar för kvot/ledtid #########
df <- read.csv2(paste0(filnamn,".txt")) %>%
lownames()
######## If else funktion som hanterar om det är län/län&region/region #########
if ('regionname' %in% names(df) & 'countyname' %in% names(df)){
df <- df %>%
mutate(countyname = ifelse(countyname != "", countyname, regionname),
countyname = ifelse(countyname == "", "Riket", countyname)
)
} else if ('regionname' %in% names(df)) {
df <- df %>%
mutate(countyname = ifelse(regionname == "", "Riket", as.character(regionname)))
} else {
df <- df %>%
mutate(countyname = ifelse(countyname == "", "Riket", as.character(countyname)))
}
################ Gör bearbetningar för specifikt för kvotledtid ################
kvot <- with(df,exists("rate"))
if(kvot) {
df <- df %>%
mutate(value = round(rate*100,2),
time = substring(measureperiodstart, 1,4)
) %>%
select(value, time, countyname, denominator) %>%
dplyr::rename(x = time,
y = value,
z = denominator)
} else {
df <- df %>%
mutate(value = value,
time = substring(measureperiodstart, 1,4)) %>%
select(value, time, countyname, population) %>%
rename(x = time,
y = value,
z = population)
}
# rownames(df) <- df[,1]
# df <- df[,-1]
# paste0(substring(measureperiodstart, 1,4), "-", substring(measureperiodend, 1,4))
################ Skapa plotten samt sätt gemensamma parametrar #################
h1 <- Highcharts$new()
h1$chart(type = "column")
unique(df$countyname)
lapply(unique(df$countyname),
function(x){
h1$series(
data = toJSONArray2(df[df$countyname == x,c('x', "y", "z")], names = T, json = F),
zIndex = 1,
name = x
)
}
)
h1$xAxis(categories = unique(df$time))
h1$params$plotOptions$series$visible <- FALSE
h1$params$plotOptions$series$marker$enabled <- FALSE
h1$params$plotOptions$series$tooltip$headerFormat <- "<b>{point.key}</b><br/>"
h1$subtitle(text = "Klicka på ett län eller riket nedan under 'redovisningsnivå' för att visa den i diagrammet. Du kan sedan sätta muspekaren över en 'redovisningsnivå' i diagrammet för att se det exakta värdet under aktuell period (värden för län som ej uppnår tröskelvärdet redovisas inte).")
h1$params$legend$title$text <- "Redovisningsnivå"
h1$params$height <- 600
h1$params$width <- 800
h1$params$tooltip$shared <- TRUE
###################### Sätt indikatorspecifika parametrar ######################
if(kvot) {
h1$title(text = titel)
h1$params$yAxis$min <- 0
h1$params$yAxis$max <- 100
h1$params$yAxis$tickInterval <- 25
h1$params$yAxis$title$text <- "Procent"
h1$params$tooltip$pointFormat <- "{series.name}: <b>Procent:</b> {point.y}% | <b>Antal</b>: {point.z} <br/>"
} else {
h1$title(text = titel)
h1$params$yAxis$min <- 0
h1$params$yAxis$tickInterval <- 25
h1$params$yAxis$title$text <- "Dagar (median)"
h1$params$tooltip$pointFormat <- "{series.name}: <b>Median:</b> {point.y} dagar | <b>Antal</b>: {point.z} <br/>"
}
h1
h1$save(paste0("~/Documents/Bitbucket/Repositories/varden-i-siffror/Godkännande av indikatorer/html/",filnamn,'.html'), standalone = TRUE)
########################## Hjälpfunktion för position ##########################
is.inca <- function(){
unname(!(Sys.info()["user"] == "christianstaf"))
}
########## Lokala förberedelser samt inladdning av funktioner på INCA ##########
if (!is.inca()) {
library(incavis)
library(rccdates)
setwd("~/Documents/Bitbucket/Repositories/varden-i-siffror/Mallar/Lunga")
if (!file.exists("df.rda")) {
df <- read.csv2("df.txt")
save(df, file = "df.rda")
} else {
load("df.rda")
}
param  <- list(start = c("2008-01-01", "2009-01-01", "2010-01-01", "2011-01-01", "2012-01-01","2013-01-01","2014-01-01", "2015-01-01"),
slut = c("2008-12-31", "2009-12-31", "2010-12-31", "2011-12-31", "2012-12-31","2013-12-31","2014-12-31", "2015-12-31") ,
KON = 0)
} else {
source("D:/R-Scripts/RCC/projekt/varden_i_siffror/source_vis.R")
}
################################ Ladda in paket ################################
library(dplyr)
################################################################################
#                                                                              #
#        Definition av funktioner som används vid indikatorberäkningar         #
#                                                                              #
################################################################################
FS_FormatDate <- function(f_var=NULL) {
return(as.Date(as.character(f_var),format="%Y-%m-%d"))
}
FS_PrepareDiaDtm <- function(f_data=df) {
f_data$a_provdtm <- FS_FormatDate(f_data$a_provdtm)
f_data$a_visadtm <- FS_FormatDate(f_data$a_visadtm)
f_diadtm <- f_data$a_provdtm
f_diadtm[is.na(f_diadtm)] <- f_data$a_visadtm[is.na(f_diadtm)]
return(f_diadtm)
}
FS_PrepareStage <- function(f_data=df,groupAB=FALSE) {
f_stage <- as.character(f_data$a_stadium)
f_stage[is.na(f_stage) | f_stage==""] <- "Uppgift saknas"
if(groupAB) {
levels1 <- "I"
levels2 <- "II"
levels3 <- "III"
} else {
levels1 <- c("IA","IB")
levels2 <- c("IIA","IIB")
levels3 <- c("IIIA","IIIB")
}
f_stage <- factor(f_stage,levels=c(levels1,levels2,levels3,"IV","Uppgift saknas"))
return(f_stage)
}
FS_PrepareDiagnos <- function(f_data=df,group=TRUE) {
f_data$a_diagnos[is.na(f_data$a_diagnos)] <- 99
if(group) {
f_diagnosgrp <- rep(99,nrow(f_data))
f_diagnosgrp[f_data$a_diagnos%in%c(1,3,4,5,6,9)] <- 1
f_diagnosgrp[f_data$a_diagnos%in%c(2)] <- 2
f_diagnosgrp[f_data$a_diagnos%in%c(7,8,91)] <- 3
f_diagnosgrp <- factor(f_diagnosgrp,levels=c(1:3,99),labels=c("NSCLC","SCLC","Övriga","Uppgift saknas"))
} else {
f_diagnosgrp <- f_data$a_diagnos
f_diagnosgrp[is.na(f_diagnosgrp)] <- 99
f_diagnosgrp <- factor(f_diagnosgrp,levels=c(1:9,91,99),labels=c("Skivepitel","Småcellig","Adenocarcinom","Storcellig/lågt diff. icke-småcellig","Adenoskvamös","Pleomorfa/sarkomatösa inslag","Carcinoid","Spottkörteltyp","Oklassificerad cancer","Cytologisk/histologisk diagnos föreligger ej","Uppgift saknas"))
}
return(f_diagnosgrp)
}
################################################################################
#                                                                              #
#                   Beräkning av urvalsramen för indikatorn                    #
#                                                                              #
################################################################################
# Fixa variabelnamn och rensa bort Region Demo
# --------------------------------------------
colnames(df) <- tolower(colnames(df))
colnames(df) <- gsub("_värde","",colnames(df))
df <- subset(df,region_namn!="Region Demo")
df <- df[order(df$pat_id,df$lopnr),]
df <- subset(df,!duplicated(data.frame(pat_id,lopnr)))
df$a_diadtm <- FS_PrepareDiaDtm()
df$a_diagnosgrp <- FS_PrepareDiagnos()
df$a_stagegrp <- FS_PrepareStage()
df$urval_period <- df$a_diadtm
df$urval_lakofo <- df$a_lkfaig
df$urval_sjukhus <- df$a_sjukhus
df$varinterest <- factor(1*(df$a_multidickonf==1),levels=c(1,0))
table(df$varinterest)
table(df$varinterest, useNA = "always")
df  <- subset(df,!is.na(varinterest))
########################## Hjälpfunktion för position ##########################
is.inca <- function(){
unname(!(Sys.info()["user"] == "christianstaf"))
}
########## Lokala förberedelser samt inladdning av funktioner på INCA ##########
if (!is.inca()) {
library(incavis)
library(rccdates)
setwd("~/Documents/Bitbucket/Repositories/varden-i-siffror/Mallar/Lunga")
if (!file.exists("df.rda")) {
df <- read.csv2("df.txt")
save(df, file = "df.rda")
} else {
load("df.rda")
}
param  <- list(start = c("2008-01-01", "2009-01-01", "2010-01-01", "2011-01-01", "2012-01-01","2013-01-01","2014-01-01", "2015-01-01"),
slut = c("2008-12-31", "2009-12-31", "2010-12-31", "2011-12-31", "2012-12-31","2013-12-31","2014-12-31", "2015-12-31") ,
KON = 0)
} else {
source("D:/R-Scripts/RCC/projekt/varden_i_siffror/source_vis.R")
}
################################ Ladda in paket ################################
library(dplyr)
################################################################################
#                                                                              #
#        Definition av funktioner som används vid indikatorberäkningar         #
#                                                                              #
################################################################################
FS_FormatDate <- function(f_var=NULL) {
return(as.Date(as.character(f_var),format="%Y-%m-%d"))
}
FS_PrepareDiaDtm <- function(f_data=df) {
f_data$a_provdtm <- FS_FormatDate(f_data$a_provdtm)
f_data$a_visadtm <- FS_FormatDate(f_data$a_visadtm)
f_diadtm <- f_data$a_provdtm
f_diadtm[is.na(f_diadtm)] <- f_data$a_visadtm[is.na(f_diadtm)]
return(f_diadtm)
}
FS_PrepareStage <- function(f_data=df,groupAB=FALSE) {
f_stage <- as.character(f_data$a_stadium)
f_stage[is.na(f_stage) | f_stage==""] <- "Uppgift saknas"
if(groupAB) {
levels1 <- "I"
levels2 <- "II"
levels3 <- "III"
} else {
levels1 <- c("IA","IB")
levels2 <- c("IIA","IIB")
levels3 <- c("IIIA","IIIB")
}
f_stage <- factor(f_stage,levels=c(levels1,levels2,levels3,"IV","Uppgift saknas"))
return(f_stage)
}
FS_PrepareDiagnos <- function(f_data=df,group=TRUE) {
f_data$a_diagnos[is.na(f_data$a_diagnos)] <- 99
if(group) {
f_diagnosgrp <- rep(99,nrow(f_data))
f_diagnosgrp[f_data$a_diagnos%in%c(1,3,4,5,6,9)] <- 1
f_diagnosgrp[f_data$a_diagnos%in%c(2)] <- 2
f_diagnosgrp[f_data$a_diagnos%in%c(7,8,91)] <- 3
f_diagnosgrp <- factor(f_diagnosgrp,levels=c(1:3,99),labels=c("NSCLC","SCLC","Övriga","Uppgift saknas"))
} else {
f_diagnosgrp <- f_data$a_diagnos
f_diagnosgrp[is.na(f_diagnosgrp)] <- 99
f_diagnosgrp <- factor(f_diagnosgrp,levels=c(1:9,91,99),labels=c("Skivepitel","Småcellig","Adenocarcinom","Storcellig/lågt diff. icke-småcellig","Adenoskvamös","Pleomorfa/sarkomatösa inslag","Carcinoid","Spottkörteltyp","Oklassificerad cancer","Cytologisk/histologisk diagnos föreligger ej","Uppgift saknas"))
}
return(f_diagnosgrp)
}
################################################################################
#                                                                              #
#                   Beräkning av urvalsramen för indikatorn                    #
#                                                                              #
################################################################################
# Fixa variabelnamn och rensa bort Region Demo
# --------------------------------------------
colnames(df) <- tolower(colnames(df))
colnames(df) <- gsub("_värde","",colnames(df))
df <- subset(df,region_namn!="Region Demo")
# Behåll endast första kombinationen av personnummer och löpnummer
# ----------------------------------------------------------------
df <- df[order(df$pat_id,df$lopnr),]
df <- subset(df,!duplicated(data.frame(pat_id,lopnr)))
# Defniera variabler
# ------------------
df$a_diadtm <- FS_PrepareDiaDtm()
df$a_diagnosgrp <- FS_PrepareDiagnos()
df$a_stagegrp <- FS_PrepareStage()
# Definiera urvalsvariabler
# -------------------------
df$urval_period <- df$a_diadtm
df$urval_lakofo <- df$a_lkfaig
df$urval_sjukhus <- df$a_sjukhus
# Variabel av intresse (täljare om =1)
# ------------------------------------
df$varinterest <- factor(1*(df$a_multidickonf==1),levels=c(1,0))
# Specifikt urval för indikatorn (nämnare)
# ----------------------------------------
df  <- subset(df,!is.na(varinterest))
########### Skapa en ny dataram med länsvariabel och rätt tidsperiod ###########
df_ungrouped <-
df %>%
prepare_df() %>%
mutate(
RegionName = region_namn,
CountyOidExtension = lkf2CountyOidExtension(urval_lakofo),
MeasurePeriodStart = date2MeasurePeriodStart(urval_period),
MeasurePeriodEnd = date2MeasurePeriodEnd(urval_period)
) %>%
group_by(MeasurePeriodStart, MeasurePeriodEnd) %>%
filter(
between_param_dates(urval_period),
!is.na(CountyOidExtension),
!is.na(MeasurePeriodStart),
if (param$KON %in% 1:2) kon_value %in% param$KON else TRUE
)
################## Skapa en ny dataram med gruppering på län ###################
df_lan <- group_by(df_ungrouped,  CountyOidExtension, add = TRUE )
#### Skapa funktion för att beräka ut kvotvärden samt första/sista mätvärde ####
summarise2 <- function(x) {
x %>%
summarise(
Denominator           = sum(varinterest %in% 0:1),
Numerator             = sum(varinterest == 1),
FirstServiceEncounter = min(urval_period, na.rm = TRUE),
LastServiceEncounter  = max(urval_period, na.rm = TRUE)
)
}
################# Slå samman olika dataramar med olika stratum #################
df_all <- bind_rows(summarise2(df_ungrouped), summarise2(df_lan))
############ Skicka in den sammanslagna dataramen i VIS-funktionen #############
vis(df_all, minexp = 5)
################################################################################
#                                                                              #
#            Skapande av inbäddad HTML fil för presentation av VIS             #
#                                                                              #
################################################################################
filnamn <- "Lunga - VIS - MDK"
titel <- filnamn
# filnamn <- "Prostata - VIS - Kontaktsjuksköterska"
# titel <- "Prostata - VIS - Kontaktsjuksköterska"
############################# Förberedande arbete ##############################
library(rcc)
library(rCharts)
library(dplyr)
library(jsonlite)
library(tidyr)
setwd("~/Documents/Bitbucket/Repositories/varden-i-siffror/Godkännande av indikatorer")
######## Ladda in fil och gör gemensamma bearbetningar för kvot/ledtid #########
df <- read.csv2(paste0(filnamn,".txt")) %>%
lownames()
######## If else funktion som hanterar om det är län/län&region/region #########
if ('regionname' %in% names(df) & 'countyname' %in% names(df)){
df <- df %>%
mutate(countyname = ifelse(countyname != "", countyname, regionname),
countyname = ifelse(countyname == "", "Riket", countyname)
)
} else if ('regionname' %in% names(df)) {
df <- df %>%
mutate(countyname = ifelse(regionname == "", "Riket", as.character(regionname)))
} else {
df <- df %>%
mutate(countyname = ifelse(countyname == "", "Riket", as.character(countyname)))
}
################ Gör bearbetningar för specifikt för kvotledtid ################
kvot <- with(df,exists("rate"))
if(kvot) {
df <- df %>%
mutate(value = round(rate*100,2),
time = substring(measureperiodstart, 1,4)
) %>%
select(value, time, countyname, denominator) %>%
dplyr::rename(x = time,
y = value,
z = denominator)
} else {
df <- df %>%
mutate(value = value,
time = substring(measureperiodstart, 1,4)) %>%
select(value, time, countyname, population) %>%
rename(x = time,
y = value,
z = population)
}
# rownames(df) <- df[,1]
# df <- df[,-1]
# paste0(substring(measureperiodstart, 1,4), "-", substring(measureperiodend, 1,4))
################ Skapa plotten samt sätt gemensamma parametrar #################
h1 <- Highcharts$new()
h1$chart(type = "column")
unique(df$countyname)
lapply(unique(df$countyname),
function(x){
h1$series(
data = toJSONArray2(df[df$countyname == x,c('x', "y", "z")], names = T, json = F),
zIndex = 1,
name = x
)
}
)
h1$xAxis(categories = unique(df$time))
h1$params$plotOptions$series$visible <- FALSE
h1$params$plotOptions$series$marker$enabled <- FALSE
h1$params$plotOptions$series$tooltip$headerFormat <- "<b>{point.key}</b><br/>"
h1$subtitle(text = "Klicka på ett län eller riket nedan under 'redovisningsnivå' för att visa den i diagrammet. Du kan sedan sätta muspekaren över en 'redovisningsnivå' i diagrammet för att se det exakta värdet under aktuell period (värden för län som ej uppnår tröskelvärdet redovisas inte).")
h1$params$legend$title$text <- "Redovisningsnivå"
h1$params$height <- 600
h1$params$width <- 800
h1$params$tooltip$shared <- TRUE
###################### Sätt indikatorspecifika parametrar ######################
if(kvot) {
h1$title(text = titel)
h1$params$yAxis$min <- 0
h1$params$yAxis$max <- 100
h1$params$yAxis$tickInterval <- 25
h1$params$yAxis$title$text <- "Procent"
h1$params$tooltip$pointFormat <- "{series.name}: <b>Procent:</b> {point.y}% | <b>Antal</b>: {point.z} <br/>"
} else {
h1$title(text = titel)
h1$params$yAxis$min <- 0
h1$params$yAxis$tickInterval <- 25
h1$params$yAxis$title$text <- "Dagar (median)"
h1$params$tooltip$pointFormat <- "{series.name}: <b>Median:</b> {point.y} dagar | <b>Antal</b>: {point.z} <br/>"
}
h1
h1$save(paste0("~/Documents/Bitbucket/Repositories/varden-i-siffror/Godkännande av indikatorer/html/",filnamn,'.html'), standalone = TRUE)
