---
title: "VIS - Prostata"
author: "Christian Staf"
date: "`r Sys.Date()`"
output:
  html_document:
    toc: true
    theme: cerulean
---
<!-- INCA LOGGA -->
<img src="img/inca.jpg" style="position:absolute;top:10px;right:0px;height: auto; width: 80px;" />
<!-- RCC/SWEgcg LOGGA -->
<img src="img/rcc_samverkan.jpg" style="position:absolute;top:-20px;right:110px;height: auto; width: 250px;" />

Klicka på ett län eller riket nedan under 'redovisningsnivå' för att visa den i diagrammet. Du kan sedan sätta muspekaren över en 'redovisningsnivå' i diagrammet för att se det exakta värdet under aktuell period (värden för län som ej uppnår tröskelvärdet redovisas inte).

Om en blank ruta visas under varje titel nedan beror det på att highcharten laddas. Detta kan ta upp till 5-10 sekunder för långsammare webbläsare (t.e.x Internet explorer 9-10).

***

```{r include= FALSE}
library(knitr)
knitr::opts_chunk$set(echo = FALSE, results = 'asis', comment = NA, message = FALSE, warning = FALSE)
```

```{r include=FALSE}
############## Dessa paket är nödvändiga för att skapa dokumentet ##############
library(dplyr)
library(tidyr)
library(rCharts)
library(jsonlite)

#### Sätt pathen till dit där .txt filerna skapat av VIS funktionen ligger #####
path <- "~/Documents/Bitbucket/Repositories/varden-i-siffror/Mallar/Prostata/"

```

```{r}
################################################################################
#                                                                              #
#                          Funktion för att skapa HC                           #
#                                                                              #
################################################################################
VIS_HC <- function(df,
                   titel = "",
                   tooltip = NULL,
                   ylab = NULL
                   ){
############################# Förberedande arbete ##############################
library(rCharts)
library(dplyr)
library(jsonlite)
library(tidyr)


######## If else funktion som hanterar om det är län/län&region/region #########
if ('regionname' %in% names(df) & 'countyname' %in% names(df)){
  df <- df %>%
    mutate(countyname = ifelse(countyname != "", as.character(countyname), as.character(regionname)),
           countyname = ifelse(countyname == "", "Riket", as.character(countyname))
    )
} else if ('regionname' %in% names(df)) {
  df <- df %>%
    mutate(countyname = ifelse(regionname == "", "Riket", as.character(regionname)))
} else {
  df <- df %>%
    mutate(countyname = ifelse(countyname == "", "Riket", as.character(countyname)))
}


################ Gör bearbetningar för specifikt för kvotledtid ################
kvot <- with(df,exists("rate"))

if(kvot) {
    df <- df %>%
        mutate(rate = ifelse(!is.na(rate), rate, 0),
               denominator = ifelse(!is.na(denominator), denominator,0),
               value = round(rate*100,2),
               time = substring(as.character(measureperiodstart), 1,4)
        ) %>%
        select(value, time, countyname, denominator) %>%
        dplyr::rename(x = time,
                      y = value,
                      z = denominator)
    

} else {
    df <- df %>%
        mutate(value = value,
               time = substring(as.character(measureperiodstart), 1,4)) %>%
        select(value, time, countyname, population) %>%
        rename(x = time,
               y = value,
               z = population)
}
# Lägga till år med 0 om NA
df <-   merge(expand.grid(x = unique(df$x),
                          countyname = unique(df$countyname)
),
df,
all = TRUE) %>% 
  mutate(y = ifelse(!is.na(y), y, 0),
         z = ifelse(!is.na(z), z, 0),
         x = as.character(x)
  )

################ Skapa plotten samt sätt gemensamma parametrar #################
h1 <- Highcharts$new()
h1$chart(type = "column")
unique(df$countyname)
lapply(unique(df$countyname),
       function(x){
         h1$series(
           data = toJSONArray2(df[df$countyname == x,c('x', "y", "z")], names = T, json = F),
           zIndex = 1,
           name = x
         )
       }
)
h1$xAxis(categories = unique(as.character(df$x)))
# h1$xAxis(type = "category")

h1$params$plotOptions$series$visible <- FALSE
h1$params$plotOptions$series$marker$enabled <- FALSE

h1$params$plotOptions$series$tooltip$headerFormat <- "<b>{point.key}</b><br/>"
h1$subtitle(text = "Klicka på ett län eller riket nedan under 'redovisningsnivå' för att visa den i diagrammet. Du kan sedan sätta muspekaren över en 'redovisningsnivå' i diagrammet för att se det exakta värdet under aktuell period (värden för län som ej uppnår tröskelvärdet redovisas inte).")
h1$params$legend$title$text <- "Redovisningsnivå"
h1$params$height <- 600
h1$params$width <- 800
h1$params$tooltip$shared <- TRUE
###################### Sätt indikatorspecifika parametrar ######################

if(kvot) {
  h1$title(text = titel)
  h1$params$yAxis$min <- 0
  h1$params$yAxis$max <- 100
  h1$params$yAxis$tickInterval <- 25
  h1$params$yAxis$title$text <- "Procent"
  h1$params$tooltip$pointFormat <- "{series.name}: <b>Procent:</b> {point.y}% | <b>Antal</b>: {point.z} <br/>"
} else {
  h1$title(text = titel)
  h1$params$yAxis$min <- 0
  h1$params$yAxis$title$text <- if (is.null(ylab)) "Dagar (median)" else ylab
  h1$params$tooltip$pointFormat <- if (is.null(tooltip)) "{series.name}: <b>Median:</b> {point.y} dagar | <b>Antal</b>: {point.z} <br/>" else tooltip

}
options(digits.secs = 6)
# h1
h1$print(gsub(" ", "",substring(Sys.time(),1,31))
, include_assets = TRUE)
}

################################################################################
#                                                                              #
#                        Loop över samtliga indikatorer                        #
#                                                                              #
################################################################################
for (i in list.files(path)[grepl(".txt", list.files(path)) & !grepl("df.txt", list.files(path)) & !grepl("output.txt", list.files(path))]){
  read.csv2(paste0(path, i)) %>%
    setNames(tolower(names(.))) %>% 
    # lownames() %>%
    VIS_HC(., titel = substring(i,1,nchar(i)-4))
}

```






